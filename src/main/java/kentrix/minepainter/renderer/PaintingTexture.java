/*
 */
package kentrix.minepainter.renderer;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

import java.util.Arrays;

import kentrix.minepainter.Config;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.ProxyBase;
import kentrix.minepainter.utils.Texture;
import net.minecraft.item.ItemDye;
import org.lwjgl.opengl.GL11;

/**
 * @author Two
 */
public class PaintingTexture extends Texture {

  public static final int DEFAULT_SIZE = ProxyBase.resolution;

  public PaintingTexture() {
    super(DEFAULT_SIZE, DEFAULT_SIZE);

    Arrays.fill(this.pixels, 0xFFFFFFFF); //WHITE
  }

  @SideOnly(Side.CLIENT)
  @Override
  protected void setFilters() {
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_LINEAR); // linear looks better on range
    GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST); // if close by show the pixels
  }

  @Override
  public String getIconName() {
    return "painting";
  }

}
