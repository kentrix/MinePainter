package kentrix.minepainter.renderer;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.painting.PaintingPlacement;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import org.lwjgl.opengl.GL11;
import kentrix.minepainter.painting.PaintingEntity;

@SideOnly(Side.CLIENT)
public class PaintingRenderer extends TileEntitySpecialRenderer {

  @Override
  public void renderTileEntityAt(final TileEntity entity, double x, double y, double z, float partial) {
    if (entity instanceof PaintingEntity) {
      final PaintingEntity pe = (PaintingEntity) entity;
      final Tessellator tes = Tessellator.instance;
      final PaintingTexture texture = pe.getTexture();
      final PaintingPlacement placement = PaintingPlacement.of(pe.getBlockMetadata());

      GL11.glPushMatrix();
      texture.bind();
      RenderHelper.disableStandardItemLighting();
      GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
      GL11.glEnable(GL11.GL_BLEND);
      GL11.glTranslated(x, y, z);

      //face
      tes.startDrawingQuads();
      addPoint(placement, 0, 0, texture);
      addPoint(placement, 0, 1, texture);
      addPoint(placement, 1, 1, texture);
      addPoint(placement, 1, 0, texture);
      tes.draw();

      GL11.glPopMatrix();
      GL11.glDisable(GL11.GL_BLEND);
      texture.unbind();
      RenderHelper.enableStandardItemLighting();
    } else {
      MinePainter.log.error("[PaintingRenderer.renderTileEntityAt]: tried to render PaintingEntity, but found %s", (entity == null ? "null" : entity.getClass().getName()));
    }
  }

  private void addPoint(PaintingPlacement pp, int x, int y, IIcon icon) {
    float[] pos = pp.painting2block(x, y, 0.003f);
    Tessellator.instance.addVertexWithUV(pos[0], pos[1], pos[2],
            x == 0 ? icon.getMinU() : icon.getMaxU(),
            y == 0 ? icon.getMinV() : icon.getMaxV());
  }
}
