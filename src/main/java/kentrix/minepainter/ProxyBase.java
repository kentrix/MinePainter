/*
 */
package kentrix.minepainter;

import java.util.LinkedList;

import kentrix.minepainter.gui.ManagerGUIHandler;
import kentrix.minepainter.item.CanvasItem;
import kentrix.minepainter.item.ItemHandle;
import kentrix.minepainter.item.Manager;
import kentrix.minepainter.painting.*;
import kentrix.minepainter.painting.paintTools.*;
import kentrix.minepainter.utils.BlockLoader;
import net.minecraftforge.common.MinecraftForge;
import kentrix.minepainter.item.Palette;

/**
 * This class will always be loaded, whether this is running as client or server.
 * All mod content that should/must be available for both sides goes here.
 *
 * @author Two
 */
public class ProxyBase {

  public static final BlockLoader<PaintingBlock> blockPainting = new BlockLoader(new PaintingBlock(), PaintingEntity.class);

  public static Manager itemManager;
  public static ManagerGUIHandler managerGUIHandler;
  public static ItemHandle itemHandle;
  public static PaintTool itemEraser;
  public static PaintTool itemBucket;
  public static PaintTool itemMixerbrush;
  public static PaintTool itemMinibrush;
  public static Palette itemPalette;
  /* Items with special renderer */
  public static CanvasItem itemCanvas;
  /* Initialization list for content that needs post-initialization. */
  protected LinkedList<InitializableModContent> pendingInitializationList = new LinkedList<InitializableModContent>();
  /* Global Config vars */
  public static boolean doAmbientOcclusion; // whether or not to do ambient occlusion during rendering
  public static int resolution;

  public ProxyBase() {
  }

  protected void loadGlobalConfigValues() {
    doAmbientOcclusion = MinePainter.config.getMiscBoolean("Render with Ambient Occlusion", false);
    resolution = MinePainter.config.getMiscInteger("resolution", 128);
  }

  protected void registerBlocks() {
    blockPainting.load();
  }

  protected void registerItems() {
    itemHandle = new ItemHandle();
    pendingInitializationList.add(itemHandle);
    itemMinibrush = new MiniBrush();
    pendingInitializationList.add(itemMinibrush);
    itemMixerbrush = new MixerBrush();
    pendingInitializationList.add(itemMixerbrush);
    itemBucket = new Bucket();
    pendingInitializationList.add(itemBucket);
    itemEraser = new Eraser();
    pendingInitializationList.add(itemEraser);
    itemPalette = new Palette();
    pendingInitializationList.add(itemPalette);
    itemCanvas = new CanvasItem();
    pendingInitializationList.add(itemCanvas);
    itemManager = new Manager();
    pendingInitializationList.add(itemManager);
    managerGUIHandler = new ManagerGUIHandler();
    pendingInitializationList.add(managerGUIHandler);
  }

  protected void registerRenderers() {
  }

  public void onPreInit() {
    MinecraftForge.EVENT_BUS.register(this);
  }

  public void onInit() {
    loadGlobalConfigValues();
    registerBlocks();
    registerItems();
    registerRenderers();

    InitializableModContent content;
    while ((content = pendingInitializationList.poll()) != null) {
      content.initialize();
    }
  }

  public void onPostInit() {
  }
}
