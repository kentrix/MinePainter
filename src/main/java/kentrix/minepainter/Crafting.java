package kentrix.minepainter;

import cpw.mods.fml.common.registry.GameRegistry;
import kentrix.minepainter.painting.paintTools.Bucket;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;

public class Crafting {

  public static final Crafting instance = new Crafting();

  public void registerRecipes() {

    if (MinePainter.config.isCraftingEnabled("MiniBrush")) {
      GameRegistry.addRecipe(new ItemStack(ProxyBase.itemMinibrush),
              "X  ", " Y ", "  Z",
              'X', new ItemStack(Blocks.wool),
              'Y', new ItemStack(Items.stick),
              'Z', new ItemStack(Items.dye, 1, 1));
    }

    if (MinePainter.config.isCraftingEnabled("MixerBrush")) {
      GameRegistry.addRecipe(new ItemStack(ProxyBase.itemMixerbrush),
              "XX ", "XY ", "  Z",
              'X', new ItemStack(Blocks.wool),
              'Y', new ItemStack(Items.stick),
              'Z', new ItemStack(Items.dye, 1, 1));
    }

    if (MinePainter.config.isCraftingEnabled("Canvas")) {
      GameRegistry.addRecipe(new ItemStack(ProxyBase.itemCanvas),
              "XXX", "XXX",
              'X', new ItemStack(Blocks.wool, 1, OreDictionary.WILDCARD_VALUE));
    }

    if (MinePainter.config.isCraftingEnabled("Handle")) {
      GameRegistry.addShapelessRecipe(new ItemStack(ProxyBase.itemHandle),
              Items.leather, Items.leather, Items.stick);
    }

    if (MinePainter.config.isCraftingEnabled("Eraser")) {
      GameRegistry.addRecipe(new ItemStack(ProxyBase.itemEraser),
              "XX ", "YY ", "ZZ ",
              'X', new ItemStack(Items.slime_ball),
              'Y', new ItemStack(Items.paper),
              'Z', new ItemStack(Items.dye, 1, 4));
    }

  }

  static final IRecipe fillBucket = new IRecipe() {
    @Override
    public int getRecipeSize() {
      return 0;
    }

    @Override
    public ItemStack getRecipeOutput() {
      return null;
    }

    @Override
    public boolean matches(InventoryCrafting ic, World w) {
      ItemStack bucket = null;
      ItemStack dye = null;

      int size = ic.getSizeInventory();
      for (int i = 0; i < size; i++) {
        ItemStack is = ic.getStackInSlot(i);
        if (is == null) {
          continue;
        }
        if (is.getItem() instanceof Bucket || is.getItem() == Items.water_bucket) {
          if (bucket != null) {
            return false;
          }
          bucket = is;
          continue;
        }
        if (is.getItem() instanceof ItemDye || is.getItem() == Items.slime_ball) {
          if (dye != null) {
            return false;
          }
          dye = is;
          continue;
        }
        return false;
      }
      return bucket != null && dye != null;
    }

    @Override
    public ItemStack getCraftingResult(InventoryCrafting ic) {
      ItemStack bucket = null;
      ItemStack dye = null;

      int size = ic.getSizeInventory();
      for (int i = 0; i < size; i++) {
        ItemStack is = ic.getStackInSlot(i);
        if (is == null) {
          continue;
        }
        if (is.getItem() instanceof Bucket || is.getItem() == Items.water_bucket) {
          if (bucket != null) {
            return null;
          }
          bucket = is;
          continue;
        }
        if (is.getItem() instanceof ItemDye || is.getItem() == Items.slime_ball) {
          if (dye != null) {
            return null;
          }
          dye = is;
          continue;
        }
      }
      ItemStack newbucket = new ItemStack(ProxyBase.itemBucket);
      newbucket.setItemDamage(dye.getItem() == Items.slime_ball ? 16 : dye.getItemDamage());
      return newbucket;
    }
  };
}
