/*
 */
package kentrix.minepainter.item;

import cpw.mods.fml.common.registry.GameRegistry;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.InitializableModContent;
import net.minecraft.item.Item;

/**
 * @author Two
 */
public class ItemBase extends Item implements InitializableModContent {

  public ItemBase() {
    this.setCreativeTab(MinePainter.tabCreative);
  }

  public String getGameRegistryName() {
    return this.getClass().getSimpleName().replace("$", "_");
  }
  
  @Override
  public void initialize() {
    GameRegistry.registerItem(this, getGameRegistryName());
  }
}
