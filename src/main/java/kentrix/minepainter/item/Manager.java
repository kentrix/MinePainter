package kentrix.minepainter.item;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.ProxyBase;
import kentrix.minepainter.network.ManagerOperationMessage;
import kentrix.minepainter.network.NBTUpdateMessage;
import kentrix.minepainter.painting.PaintingBlock;
import kentrix.minepainter.painting.PaintingEntity;
import kentrix.minepainter.painting.PaintingPlacement;
import kentrix.minepainter.utils.ImgUtils;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.util.ForgeDirection;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

public class Manager extends ItemBase {

    public Manager() {
        super();
        this.setMaxStackSize(1);
        setUnlocalizedName("Manager");
        setTextureName("minepainter:handle");
    }

    @Override
    public boolean onItemUse(ItemStack is, EntityPlayer ep, World w, int x, int y, int z, int face, float xs, float ys, float zs) {

        if(w.isRemote) {
            //Logical Client
            seekStructure(w, x, y, z);
            if(!(w.getBlock(x, y, z) instanceof PaintingBlock)) {
                return false;
            }
            else {
                ep.openGui(MinePainter.instance, 0, w, x, y, z);
                return false;
            }
        }


        MinePainter.log.info(" " + x + " " + y+ " "  + z +  " " + w.getBlock(x,y,z));
        return true;
    }

    /*
    @Override
    public ItemStack onItemRightClick(ItemStack is, World world, EntityPlayer ep) {
        if(world.isRemote) {
            ep.openGui(MinePainter.instance, 0, world, (int) ep.posX, (int) ep.posY, (int) ep.posZ);
        }
        return is;
    }
    */

    public static void fillEntities(World world, int x, int y, int z, boolean scale, String url) {

        if(!world.isRemote) {

            MinePainter.log.info("Processing " + x + "," + y + "," + z + "," + url + "scale:" + scale);

            int[][][] ret = seekStructure(world, x, y, z);
            int row = ret.length;
            int col = ret[0].length;
            int meta = world.getBlockMetadata(x, y, z);

            BufferedImage downloadedImage;
            try {
                downloadedImage = ImgUtils.readFromLocation(url);
                if(downloadedImage == null) {
                    return;
                }
                final NBTTagCompound[][] nbtTags = ImgUtils.getNBTTagsFromImage(downloadedImage, col, row, scale);
                for (int height = 0; height < row; height++) {
                    for (int width = 0; width < col; width++) {
                        Block blk = world.getBlock(x, y, z);
                        if(!(blk instanceof PaintingBlock)) {
                            world.setBlockToAir(ret[height][width][0], ret[height][width][1], ret[height][width][2]);
                            world.setBlock(ret[height][width][0], ret[height][width][1] , ret[height][width][2], ProxyBase.blockPainting.getBlock() , meta, 3);
                        }
                        else {
                            final PaintingEntity pe = new PaintingEntity();
                            pe.readFromNBT(nbtTags[height][width]);
                            world.setTileEntity(ret[height][width][0], ret[height][width][1], ret[height][width][2], pe);
                            world.markBlockForUpdate(ret[height][width][0], ret[height][width][1], ret[height][width][2]);
                            pe.markDirty();
                        }

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private static int[][][] seekStructure(World world, int x, int y, int z) {
        Block blk;
        int structureR = 1;
        int structureC = 1;

        blk = world.getBlock(x, y, z);
        int blkMeta = world.getBlockMetadata(x, y, z);
        PaintingPlacement pp = PaintingPlacement.of(blkMeta);
        ForgeDirection dirX = pp.getXpos();
        ForgeDirection dirY = pp.getYpos();

        if(blk instanceof PaintingBlock) {


            //TODO: max to search
            for(int rSearch = 1; rSearch <= 256; rSearch++) {
                Block iBlock = world.getBlock(x + dirX.offsetX * rSearch , y + dirX.offsetY * rSearch,
                        z + dirX.offsetZ * rSearch);
                if(iBlock instanceof PaintingBlock && world.getBlockMetadata(x + dirX.offsetX * rSearch , y + dirX.offsetY * rSearch,
                        z + dirX.offsetZ * rSearch) == blkMeta) {
                    structureR++;
                }
                else {
                    break;
                }
            }

            for(int cSearch = 1; cSearch <= 256; cSearch++) {
                Block iBlock = world.getBlock(x + dirY.offsetX * cSearch , y + dirY.offsetY * cSearch,
                        z + dirY.offsetZ * cSearch);
                if(iBlock instanceof PaintingBlock && world.getBlockMetadata(x + dirY.offsetX * cSearch , y + dirY.offsetY * cSearch,
                        z + dirY.offsetZ * cSearch) == blkMeta) {
                    structureC++;
                }

                else {
                    break;
                }
            }
        }

        MinePainter.log.warn("Struct has a size of:" + structureR + "x" + structureC);

        int ret[][][] = new int[structureC][structureR][3];
        for(int c = 0; c < structureC; c++) {
            for(int r = 0; r < structureR; r++) {
                ret[c][r][0] = x + dirX.offsetX * r + dirY.offsetX * c;
                ret[c][r][1] = y + dirX.offsetY * r + dirY.offsetY * c;
                ret[c][r][2] = z + dirX.offsetZ * r + dirY.offsetZ * c;
            }
        }

        return ret;

    }
}
