/*
 */
package kentrix.minepainter;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

/**
 * @author Two
 */
public class MinePainterCreativeTab extends CreativeTabs {

  public MinePainterCreativeTab() {
    super(MinePainter.MOD_NAME);
  }

  @Override
  public Item getTabIconItem() {
    return ProxyBase.itemMixerbrush;
  }
}
