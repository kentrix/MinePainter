/*
 */
package kentrix.minepainter;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import kentrix.minepainter.painting.paintTools.PaintTool;
import kentrix.minepainter.painting.PaintingBlock;
import kentrix.minepainter.painting.PaintingPlacement;
import kentrix.minepainter.renderer.CanvasRenderer;
import kentrix.minepainter.renderer.PaintingTexture;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraftforge.client.MinecraftForgeClient;
import net.minecraftforge.client.event.DrawBlockHighlightEvent;
import net.minecraftforge.client.event.RenderWorldEvent;
import kentrix.minepainter.renderer.PaintingRenderer;

/**
 * This class will only be loaded if this is running in client mode.
 * All client-only initialization (rendering, sound, ...) goes here.
 *
 * @author Two
 */
public class ProxyClient extends ProxyBase {

  @Override
  protected void registerRenderers() {
    blockPainting.registerRendering(null, new PaintingRenderer());

    MinecraftForgeClient.registerItemRenderer(itemCanvas, new CanvasRenderer());
  }

  @SubscribeEvent
  public void onClientRender(RenderWorldEvent.Pre event) {
    Runnable r;
    while ((r = MinePainter.glTasks.poll()) != null) {
      try {
        r.run();
      } catch (Throwable t) {
        MinePainter.log.error("[ProxyClient.onClientRender] failed", t);
      }
    }
  }

  /*
  @SubscribeEvent
  public void onDrawBlockhightlight(DrawBlockHighlightEvent event) {
    final ItemStack is = event.player.getCurrentEquippedItem();
    if (is == null) {
      return;
    }
    final Item item = is.getItem();
    if ((item == null) || !(item instanceof ChiselItem)) {
      return;
    }
    final ChiselItem ci = (ChiselItem) item;

    final int x = event.target.blockX;
    final int y = event.target.blockY;
    final int z = event.target.blockZ;
    final Block sculpture = event.player.worldObj.getBlock(x, y, z);

    final int[] pos = Operations.raytrace(x, y, z, event.player);
    if (pos[0] == -1) {
      return;
    }

    int flags = ci.getChiselFlags(event.player);
    if (!Operations.validOperation(event.player.worldObj, x, y, z, pos, flags)) {
      return;
    }

    Operations.setBlockBoundsFromRaytrace(pos, sculpture, flags);
    event.context.drawSelectionBox(event.player, event.target, 0, event.partialTicks);
    sculpture.setBlockBounds(0, 0, 0, 1, 1, 1);

//		event.setCanceled(true);
  }
  */

  @SubscribeEvent
  public void onDrawPaintingPixel(DrawBlockHighlightEvent event) {
    ItemStack is = event.player.getCurrentEquippedItem();
    if (is == null || !(is.getItem() instanceof PaintTool)) {
      return;
    }

    final int x = event.target.blockX;
    final int y = event.target.blockY;
    final int z = event.target.blockZ;
    final World world = event.player.worldObj;
    final Block block = world.getBlock(x, y, z);

    if (block instanceof PaintingBlock) {
      final PaintingBlock painting = (PaintingBlock) block;
      PaintingPlacement pp = PaintingPlacement.of(world.getBlockMetadata(x, y, z));

      Vec3 pos = event.player.getPosition(1.0f);
      Vec3 look = event.player.getLookVec();
      look = pos.addVector(look.xCoord * 5, look.yCoord * 5, look.zCoord * 5);

      MovingObjectPosition mop = painting.collisionRayTrace(world, x, y, z, pos, look);
      if (mop == null) {
        return;
      }
      float[] point = pp.block2painting((float) (mop.hitVec.xCoord - mop.blockX),
              (float) (mop.hitVec.yCoord - mop.blockY),
              (float) (mop.hitVec.zCoord - mop.blockZ));

      float[] bound1 = pp.painting2block(point[0], point[1], 0.002f);
      float[] bound2 = pp.painting2block(point[0] + 1 / PaintingTexture.DEFAULT_SIZE, point[1] + 1 / PaintingTexture.DEFAULT_SIZE, 0.002f);

      painting.setBlockBounds(Math.min(bound1[0], bound2[0]),
              Math.min(bound1[1], bound2[1]),
              Math.min(bound1[2], bound2[2]),
              Math.max(bound1[0], bound2[0]),
              Math.max(bound1[1], bound2[1]),
              Math.max(bound1[2], bound2[2]));
      painting.ignore_bounds_on_state = true;
      event.context.drawSelectionBox(event.player, event.target, 0, event.partialTicks);
      painting.ignore_bounds_on_state = false;
    }
  }
}
