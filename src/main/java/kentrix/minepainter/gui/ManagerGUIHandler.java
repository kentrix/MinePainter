package kentrix.minepainter.gui;

import cpw.mods.fml.common.network.IGuiHandler;
import cpw.mods.fml.common.network.NetworkRegistry;
import kentrix.minepainter.InitializableModContent;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.utils.IDAllocator;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;

public class ManagerGUIHandler implements IGuiHandler, InitializableModContent{

    public static final int GUI_H_ID = IDAllocator.getInstance().getNextID();

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if(ID == GUI_H_ID) ;
        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        //TODO RayTrace
        //x y z are player location
        if(ID == GUI_H_ID)
            return new MangerGUI(x, y, z);
        return null;
    }

    @Override
    public void initialize() {
        NetworkRegistry.INSTANCE.registerGuiHandler(MinePainter.instance, new ManagerGUIHandler());
    }
}
