package kentrix.minepainter.gui;

import kentrix.minepainter.MinePainter;
import kentrix.minepainter.item.Manager;
import kentrix.minepainter.network.ManagerOperationMessage;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.network.NettyEncryptingDecoder;

import java.io.IOException;

/**
 * Created by you on 27/11/2016.
 */
public class MangerGUI extends GuiScreen{

    private int x;
    private int y;
    private int z;
    private GuiTextField text;
    private boolean isScalingOn = true;


    public MangerGUI(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void drawScreen(int p_73863_1_, int p_73863_2_, float p_73863_3_) {
        this.drawDefaultBackground();
        this.text.drawTextBox();
        super.drawScreen(p_73863_1_, p_73863_2_, p_73863_3_);
    }

    @Override
    protected void actionPerformed(GuiButton button){
        if(button.id == 0) {
            MinePainter.log.debug("Sending server a message " + this.getClass().getSimpleName());
            MinePainter.network.sendToServer(new ManagerOperationMessage(x, y, z, isScalingOn, text.getText()));
        }
        else if(button.id == 2){
            if(isScalingOn)
                button.displayString = "§cScale Image Off";
            else
                button.displayString =  "§aScale Image On";

            isScalingOn = !isScalingOn;
        }

        if(button.id <= 1) {
            this.mc.displayGuiScreen(null);
            this.mc.setIngameFocus();
        }
    }

    @Override
    public void updateScreen()
    {
        super.updateScreen();
        this.text.updateCursorCounter();
    }

    @Override
    protected void keyTyped(char par1, int par2)
    {
        super.keyTyped(par1, par2);
        this.text.textboxKeyTyped(par1, par2);
    }

    @Override
    protected void mouseClicked(int x, int y, int btn) {
        super.mouseClicked(x, y, btn);
        this.text.mouseClicked(x, y, btn);
    }

    @Override
    public void initGui() {
        this.text = new GuiTextField(this.fontRendererObj, this.width / 2 - 90, this.height/2 - 46, 180, 20);
        text.setMaxStringLength(256);
        text.setText("");
        this.text.setFocused(true);
        buttonList.add(new GuiButton(0, this.width / 2 - 60, this.height / 2 , 50, 20, "Apply"));
        buttonList.add(new GuiButton(1, this.width / 2 + 10, this.height / 2 , 50, 20, "Cancel"));

        buttonList.add(new GuiButton(2, this.width / 2 - 110, this.height / 2 + 40 , 80, 20, "§aScale Image On"));
        buttonList.add(new GuiButton(3, this.width / 2 + 30, this.height / 2 + 40 , 80, 20, "§aReserved"));

    }
}
