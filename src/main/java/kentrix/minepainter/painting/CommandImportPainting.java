package kentrix.minepainter.painting;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;

import kentrix.minepainter.MinePainter;
import kentrix.minepainter.ProxyBase;
import kentrix.minepainter.renderer.PaintingTexture;
import kentrix.minepainter.utils.ImgUtils;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.StatCollector;

public class CommandImportPainting extends CommandBase {

    @Override
    public String getCommandName() {
        return "imageImport";
    }

    protected static final List<String> aliases = Arrays.asList(new String[]{"iI"});

    @Override
    public List getCommandAliases() {
        return aliases;
    }

    @Override
    public String getCommandUsage(ICommandSender var1) {
        return StatCollector.translateToLocal("msg.imageDownload.usage");
    }

    @Override
    public void processCommand(ICommandSender sender, String[] message) {
        if ((message == null) || (message.length == 0) || (message.length == 2) || (message.length > 3)) {
            sender.addChatMessage(new ChatComponentTranslation("msg.imageDownload.usage"));
            return;
        }
        int w = 1, h = 1;
        final String locationStr = message[0];
        try {
            if (message.length == 3) {
                w = Integer.parseInt(message[1]);
                h = Integer.parseInt(message[2]);
            }
            if ((w > 9 * 4) || (h > 9 * 4) || (w * h > 9 * 4)) {
                sender.addChatMessage(new ChatComponentTranslation("msg.imageImageTooLarge.txt"));
            } else {
                final BufferedImage downloadedImage = ImgUtils.readFromLocation(locationStr);

                if (downloadedImage == null) {
                    sender.addChatMessage(new ChatComponentTranslation("msg.imageDownloadFailedNoImage.txt", locationStr));
                } else {
                    NBTTagCompound[][] nbtTags = ImgUtils.getNBTTagsFromImage(downloadedImage, w, h, true);
                    for (int height = 0; height < nbtTags.length; height++) {
                        for (int width = 0; width < nbtTags[0].length; width++) {
                            final ItemStack is = new ItemStack(ProxyBase.itemCanvas);

                            is.setTagCompound(nbtTags[height][width]);

                            final EntityItem entityitem = new EntityItem(sender.getEntityWorld(),
                                    sender.getPlayerCoordinates().posX + 0.5f,
                                    sender.getPlayerCoordinates().posY + 0.5f,
                                    sender.getPlayerCoordinates().posZ + 0.5f, is);

                            entityitem.delayBeforeCanPickup = 0;
                            sender.getEntityWorld().spawnEntityInWorld(entityitem);
                        }
                    }
                }
            }
    } catch (IOException e) {
      sender.addChatMessage(new ChatComponentTranslation("msg.imageDownloadFailed.txt", locationStr, e.getLocalizedMessage()));
    } catch (NumberFormatException e) {
      sender.addChatMessage(new ChatComponentTranslation("msg.imageDownloadFailed.txt", locationStr, e.getLocalizedMessage()));
    } catch (Throwable t) {
      sender.addChatMessage(new ChatComponentTranslation("msg.imageDownloadFailed.txt", locationStr, "internal error"));
      MinePainter.log.info("Dowload of image '" + String.valueOf(locationStr) + "' failed: ", t);
    }
  }

}
