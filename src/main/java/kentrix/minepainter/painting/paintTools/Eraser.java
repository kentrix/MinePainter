package kentrix.minepainter.painting.paintTools;

import kentrix.minepainter.renderer.PaintingTexture;
import kentrix.minepainter.utils.PixelData;

/**
 * Created by you on 27/11/2016.
 */
public class Eraser extends MixerBrush {

  public Eraser() {
    super();
    this.setUnlocalizedName("eraser").setTextureName("minepainter:eraser");
  }

  @Override
  public boolean apply(final PixelData img, float[] point, int color, boolean isSneaking) {

    int x = (int) (point[0] * PaintingTexture.DEFAULT_SIZE);
    int y = (int) (point[1] * PaintingTexture.DEFAULT_SIZE);
    int mixColor;

    boolean changed = false;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        if (isSneaking && (i != 0 || j != 0)) {
          continue;
        }
        if (!inBounds(x + i, y + j)) {
          continue;
        }
        changed = true;

        mixColor = img.getRGB(x + i, y + j);
        int a75 = (int) (((mixColor >> 24) & 0xff) * 0.75f) << 24;
        a75 += mixColor & 0xffffff;
        int a50 = (int) (((mixColor >> 24) & 0xff) * 0.5f) << 24;
        a50 += mixColor & 0xffffff;

        int to_blend = Math.abs(i) + Math.abs(j);
        if (to_blend == 0) {
          to_blend = 0;
        } else if (to_blend == 1) {
          to_blend = a50;
        } else {
          to_blend = a75;
        }

        img.setRGB(x + i, y + j, to_blend);
      }
    }
    return changed;
  }
}
