package kentrix.minepainter.painting.paintTools;

import kentrix.minepainter.renderer.PaintingTexture;
import kentrix.minepainter.utils.PixelData;

/**
 * Created by you on 27/11/2016.
 */
public class MixerBrush extends PaintTool {

  public MixerBrush() {
    super();
    this.setUnlocalizedName("mixer_brush").setTextureName("minepainter:brush");
  }

  @Override
  public boolean apply(final PixelData img, float[] point, int color, boolean isSneaking) {

    int x = (int) (point[0] * PaintingTexture.DEFAULT_SIZE);
    int y = (int) (point[1] * PaintingTexture.DEFAULT_SIZE);

    int a75 = (int) (((color >> 24) & 0xff) * 0.75f) << 24;
    a75 += color & 0xffffff;
    int a50 = (int) (((color >> 24) & 0xff) * 0.5f) << 24;
    a50 += color & 0xffffff;

    boolean changed = false;
    for (int i = -1; i <= 1; i++) {
      for (int j = -1; j <= 1; j++) {
        if (!inBounds(x + i, y + j)) {
          continue;
        }
        changed = true;

        int to_blend = Math.abs(i) + Math.abs(j);
        if (to_blend == 0) {
          to_blend = color;
        } else if (to_blend == 1) {
          to_blend = a75;
        } else {
          to_blend = a50;
        }

        img.setRGB(x + i, y + j, mix(to_blend, img.getRGB(x + i, y + j)));
      }
    }
    return changed;
  }

  private int mix(int color, int original) {
    float a_alpha = (color >> 24 & 0xFF) / 255.0F;
    float b_alpha = (original >> 24 & 0xFF) / 255.0F;
    float c_alpha = a_alpha + b_alpha * (1.0F - a_alpha);
    int result = 0;

    for (int b = 0; b < 24; b += 8) {
      int ca = color >> b & 0xFF;
      ca = (int) (ca * a_alpha);
      int cb = original >> b & 0xFF;
      cb = (int) (cb * b_alpha * (1.0F - a_alpha));
      result += ((int) ((ca + cb) / c_alpha) << b);
    }

    result += ((int) (255.0F * c_alpha) << 24);
    return result;
  }

}
