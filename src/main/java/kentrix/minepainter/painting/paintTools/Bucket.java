package kentrix.minepainter.painting.paintTools;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import kentrix.minepainter.renderer.PaintingTexture;
import kentrix.minepainter.utils.PixelData;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Items;
import net.minecraft.item.ItemDye;
import net.minecraft.item.ItemStack;
import net.minecraft.util.IIcon;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;

/**
 * Created by you on 27/11/2016.
 */
public class Bucket extends PaintTool {

  IIcon fill;

  public Bucket() {
    super();
    this.setUnlocalizedName("paint_bucket");
    this.setTextureName("minepainter:bucket");
    this.setHasSubtypes(true);
  }

  @Override
  public ItemStack onItemRightClick(ItemStack is, World w, EntityPlayer ep) {
    MovingObjectPosition mop = this.getMovingObjectPositionFromPlayer(w, ep, true);
    if (mop == null) {
      return is;
    }
    Material m = w.getBlock(mop.blockX, mop.blockY, mop.blockZ).getMaterial();
    if (m.isLiquid()) {
      return new ItemStack(Items.bucket);
    }
    return is;
  }

  @Override
  public boolean requiresMultipleRenderPasses() {
    return true;
  }

  @Override
  public int getRenderPasses(int metadata) {
    return 2;
  }

  @Override
  public IIcon getIcon(ItemStack is, int renderPass) {
    if (renderPass == 0) {
      return itemIcon;
    }
    return fill;
  }

  @SideOnly(Side.CLIENT)
  @Override
  public void registerIcons(IIconRegister par1IconRegister) {
    super.registerIcons(par1IconRegister);
    fill = par1IconRegister.registerIcon("minepainter:bucket_fill");
  }

  @Override
  public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
    if (par2 == 1) {
      return getColor(par1ItemStack.getItemDamage(), 0);
    }
    return super.getColorFromItemStack(par1ItemStack, par2);
  }

  @Override
  public int getColor(EntityPlayer ep, ItemStack is) {
    return getColor(is.getItemDamage(), 0xff000000);
  }

  private int getColor(int dmg, int mask) {
    if (dmg < 16) {
      return ItemDye.field_150922_c[dmg] | mask;
    }
    return 0xffffff;
  }

  @Override
  public boolean apply(final PixelData img, float[] point, int color, boolean isSneaking) {
    int x = (int) (point[0] * PaintingTexture.DEFAULT_SIZE);
    int y = (int) (point[1] * PaintingTexture.DEFAULT_SIZE);

    if (inBounds(x, y)) {
      img.fillRGB(color);
      return true;
    }
    return false;
  }
}
