package kentrix.minepainter.painting.paintTools;

import kentrix.minepainter.painting.PaintingEntity;
import kentrix.minepainter.painting.PaintingPlacement;
import kentrix.minepainter.renderer.PaintingTexture;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.ProxyBase;
import kentrix.minepainter.item.ItemBase;
import kentrix.minepainter.item.Palette;
import kentrix.minepainter.network.PaintingOperationMessage;
import kentrix.minepainter.utils.PixelData;
import kentrix.minepainter.utils.Utils;

public abstract class PaintTool extends ItemBase {

  public PaintTool() {
    this.setMaxStackSize(1);
  }

  @Override
  public boolean onItemUse(ItemStack is, EntityPlayer ep, World w, int x, int y, int z, int face, float xs, float ys, float zs) {

    if (!w.isRemote) {
      return false;
    }

    boolean changed = paintAt(w, x, y, z, xs, ys, zs, getColor(ep, is), ep.isSneaking());

    if (changed) {
      MinePainter.network.sendToServer(new PaintingOperationMessage(this, x, y, z, xs, ys, zs, getColor(ep, is)));
    }

    return changed;
  }

  public int getColor(EntityPlayer ep, ItemStack is) {
    int size = ep.inventory.getSizeInventory();
    for (int i = 0; i < size; i++) {
      ItemStack slot = ep.inventory.getStackInSlot(i);
      if (slot == null) {
        continue;
      }
      if (!(slot.getItem() instanceof Palette)) {
        continue;
      }

      return Palette.getColors(slot)[0];
    }
    return 0;
  }

  public boolean apply(PixelData img, float[] point, int color, boolean isSneaking) {
    return false;
  }

  public boolean inBounds(int x, int y) {
    return x >= 0 && x < PaintingTexture.DEFAULT_SIZE && y >= 0 && y < PaintingTexture.DEFAULT_SIZE;
  }

  public boolean paintAt(final World world, int x, int y, int z, float xs, float ys, float zs, int color, boolean isSneaking) {
    if (world.getBlock(x, y, z) != ProxyBase.blockPainting.getBlock()) {
      return false;
    }
    final TileEntity tileEntity = world.getTileEntity(x, y, z);
    if (tileEntity instanceof PaintingEntity) {
      final PaintingPlacement place = PaintingPlacement.of(world.getBlockMetadata(x, y, z));
      final float[] point = place.block2painting(xs, ys, zs);

      boolean changed = false;
      for (int rotX = -1; rotX <= 1; rotX++) {
        for (int rotY = -1; rotY <= 1; rotY++) {
          final int _x = x + place.getXpos().offsetX * rotX + place.getYpos().offsetX * rotY;
          final int _y = y + place.getXpos().offsetY * rotX + place.getYpos().offsetY * rotY;
          final int _z = z + place.getXpos().offsetZ * rotX + place.getYpos().offsetZ * rotY;

          if (world.getBlock(_x, _y, _z) != ProxyBase.blockPainting.getBlock()) {
            continue;
          }
          if (world.getBlockMetadata(_x, _y, _z) != place.ordinal()) {
            continue;
          }

          final TileEntity _tileEntity = world.getTileEntity(_x, _y, _z);
          if (_tileEntity instanceof PaintingEntity) {
            final PaintingEntity painting = (PaintingEntity) _tileEntity;

            point[0] -= rotX;
            point[1] -= rotY;
            boolean _changed = apply(painting.getTexture(), point, color, isSneaking);
            point[0] += rotX;
            point[1] += rotY;

            if (_changed) {
              if (world.isRemote == false) {
                world.markBlockForUpdate(_x, _y, _z);
              }
              changed = true;
            }
          } else {
            MinePainter.log.warn("[PaintTool.paintAt subpixel] failed: expected PaintingEntity at %d, %d, %d, but got %s", _x, _y, _z, Utils.getClassName(tileEntity));
          }
        }
      }
      return changed;
    } else {
      MinePainter.log.warn("[PaintTool.paintAt] failed: expected PaintingEntity at %d, %d, %d, but got %s", x, y, z, Utils.getClassName(tileEntity));
      return false;
    }
  }

}
