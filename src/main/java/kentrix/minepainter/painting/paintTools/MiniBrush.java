package kentrix.minepainter.painting.paintTools;

import kentrix.minepainter.renderer.PaintingTexture;
import kentrix.minepainter.utils.PixelData;

/**
 * Created by you on 27/11/2016.
 */
public class MiniBrush extends PaintTool {

  public MiniBrush() {
    super();
    this.setUnlocalizedName("mini_brush");
    this.setTextureName("minepainter:brush_small");
  }

  @Override
  public boolean apply(final PixelData pixelData, float[] point, int color, boolean isSneaking) {

    int x = (int) (point[0] * PaintingTexture.DEFAULT_SIZE);
    int y = (int) (point[1] * PaintingTexture.DEFAULT_SIZE);

    if (!inBounds(x, y)) {
      return false;
    }

    pixelData.setRGB(x, y, color);
    return true;
  }
}
