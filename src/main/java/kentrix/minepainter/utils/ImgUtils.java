package kentrix.minepainter.utils;

import kentrix.minepainter.MinePainter;
import kentrix.minepainter.ProxyBase;
import kentrix.minepainter.renderer.PaintingTexture;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.Buffer;

public class ImgUtils {

    public static BufferedImage[][] getTexturesOfPainting(BufferedImage bufferedImage, int width, int height) {

        BufferedImage[][] textures = new BufferedImage[height][width];
        final BufferedImage centeredImage = new BufferedImage(PaintingTexture.DEFAULT_SIZE * width, PaintingTexture.DEFAULT_SIZE * height, BufferedImage.TYPE_INT_ARGB);
        final Image scaledImage = getScaledImage(bufferedImage, centeredImage.getWidth(), centeredImage.getHeight());
        final int x = Math.max(0, (centeredImage.getWidth() - scaledImage.getWidth(null)) / 2);
        final int y = Math.max(0, (centeredImage.getHeight() - scaledImage.getHeight(null)) / 2);
        centeredImage.getGraphics().drawImage(scaledImage, x, y, scaledImage.getWidth(null), scaledImage.getHeight(null), null);

        for (int h = 0; h < height; h++)  {
            for (int w = 0;  w< width; w++) {
                final BufferedImage slot = centeredImage.getSubimage(w * PaintingTexture.DEFAULT_SIZE , h * PaintingTexture.DEFAULT_SIZE , PaintingTexture.DEFAULT_SIZE , PaintingTexture.DEFAULT_SIZE);
                textures[h][w] = slot;

            }
        }
        return textures;
    }

    public static NBTTagCompound[][] getNBTTagsFromImage(BufferedImage bufferedImage, int width,
                                                         int height, boolean scale) {
        NBTTagCompound[][] nbtTags = new NBTTagCompound[height][width];
        final BufferedImage centeredImage = new BufferedImage(PaintingTexture.DEFAULT_SIZE * width, PaintingTexture.DEFAULT_SIZE * height, BufferedImage.TYPE_INT_ARGB);

        if(scale) {
            final Image scaledImage;
            scaledImage = getScaledImage(bufferedImage, centeredImage.getWidth(), centeredImage.getHeight());
            final int x = Math.max(0, (centeredImage.getWidth() - scaledImage.getWidth(null)) / 2);
            final int y = Math.max(0, (centeredImage.getHeight() - scaledImage.getHeight(null)) / 2);
            centeredImage.getGraphics().drawImage(scaledImage, x, y, scaledImage.getWidth(null), scaledImage.getHeight(null), null);
        }
        else {
            centeredImage.getGraphics().drawImage(bufferedImage, (centeredImage.getWidth() - bufferedImage.getWidth()) / 2,
                    (centeredImage.getHeight() - bufferedImage.getWidth()) / 2, bufferedImage.getTileWidth(), bufferedImage.getHeight(), null);
        }


        for (int h = 0; h < height; h++)  {
            for (int w = 0;  w < width; w++) {
                final BufferedImage slot = centeredImage.getSubimage(w * PaintingTexture.DEFAULT_SIZE , h * PaintingTexture.DEFAULT_SIZE , PaintingTexture.DEFAULT_SIZE , PaintingTexture.DEFAULT_SIZE);
                final ByteArrayOutputStream baos = new ByteArrayOutputStream();
                try {
                    ImageIO.write(slot, "png", baos);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                final NBTTagCompound nbt = new NBTTagCompound();
                nbt.setByteArray("image_data", baos.toByteArray());
                nbtTags[h][w] = nbt;

            }
        }
        return nbtTags;
    }


    public static Image getScaledImage(final BufferedImage source, final int widthMax, final int heightMax) {
            final double scale = ((double) Math.min(widthMax, heightMax)) / ((double) Math.max(source.getWidth(), source.getHeight()));
            return source.getScaledInstance((int) (scale * source.getWidth()), (int) (scale * source.getHeight()), Image.SCALE_SMOOTH);
    }


    public static BufferedImage readFromLocation(final String locationStr) throws IOException {
        MinePainter.log.info("Reading from string: " + locationStr);

        if (locationStr.startsWith("http://") || locationStr.startsWith("https://")) {
            return ImageIO.read(new URL(locationStr));
        }
        else {
            return ImageIO.read(new File(locationStr));
        }

    }
}
