package kentrix.minepainter.utils;

public class IDAllocator {

    private static final IDAllocator INSTANCE = new IDAllocator();
    private int id = 0;

    public synchronized int getNextID() {
        if(id == Integer.MAX_VALUE) {
            throw new IllegalStateException("Too many ID were allocated.");
        }
        return id++;
    }

    public static synchronized IDAllocator getInstance() {
        return INSTANCE;
    }

}
