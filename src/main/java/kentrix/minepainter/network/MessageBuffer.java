package kentrix.minepainter.network;

import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import kentrix.minepainter.painting.PaintingEntity;
import net.minecraft.client.Minecraft;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.play.server.S35PacketUpdateTileEntity;
import org.apache.commons.lang3.ArrayUtils;
import scala.collection.immutable.Stream;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;

public class MessageBuffer extends NBTUpdateMessage{

    private MessageContext ctx;
    private boolean valid;

    public MessageBuffer(int x, int y, int z, int count, int of, byte[] bytes, MessageContext ctx) {
        super(x, y, z, count, of, bytes);
        if(count != 0) {
            throw new IllegalStateException("Unexpected message received.");
        }
        this.ctx = ctx;
        this.valid = true;

        if(of == 1) {
            PaintingEntity pe = (PaintingEntity) Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z);
            ByteArrayInputStream bais = new ByteArrayInputStream(bytes_data);
            BufferedImage image = null;
            try {
                image = ImageIO.read(bais);
            } catch (IOException e) {
                e.printStackTrace();
            }
            NBTTagCompound tag = new NBTTagCompound();
            PaintingEntity.imageToNBT(image, tag);
            tag.setInteger("x", x);
            tag.setInteger("y", y);
            tag.setInteger("z", z);
            tag.setString("id", "PaintingEntity");
            Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z).onDataPacket(null, new S35PacketUpdateTileEntity(x, y, z, 17, tag));
            valid = false;
        }
    }

    public void concatMessage(NBTUpdateMessage message) {
        if(message.count > this.of || message.count != count+1
                || message.x != this.x || message.y != this.y || message.z != this.z) {
            throw new IllegalStateException("Unexpected message received!" + String.format("%d %d %d %d %d", x ,y ,z ,count ,of));
        }
        if(this.count == this.of) {
            throw new IllegalStateException("Unexpected message length.");
        }

        count++;
        this.bytes_data = ArrayUtils.addAll(this.bytes_data, message.bytes_data);

        if(this.count >= this.of - 1) {
            this.valid = false;
            ByteArrayInputStream bais = new ByteArrayInputStream(this.bytes_data);
            BufferedImage image = null;
            try {
                image = ImageIO.read(bais);
            } catch (IOException e) {
                e.printStackTrace();
            }
            NBTTagCompound tag = new NBTTagCompound();
            PaintingEntity.imageToNBT(image, tag);
            tag.setInteger("x", x);
            tag.setInteger("y", y);
            tag.setInteger("z", z);
            tag.setString("id", "PaintingEntity");
            Minecraft.getMinecraft().theWorld.getTileEntity(x, y, z).onDataPacket(null, new S35PacketUpdateTileEntity(x, y, z, 17, tag));
            //pe.updateTexture(image);
            //PaintingEntity.imageToNBT(image, tag);
            //pe.readFromNBT(tag);
        }

    }

    public boolean getValidity() {
        return valid;
    }

}
