package kentrix.minepainter.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;

public class ManagerOperationMessage implements IMessage {

    int x;
    int y;
    int z;
    boolean scale;
    String url;


    public ManagerOperationMessage() {}

    public ManagerOperationMessage(int x, int y, int z, boolean scale, String url) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.scale = scale;
        this.url = url;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.scale = buf.readBoolean();
        url = ByteBufUtils.readUTF8String(buf);

    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeBoolean(scale);
        ByteBufUtils.writeUTF8String(buf, url);
    }
}
