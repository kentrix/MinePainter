package kentrix.minepainter.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import kentrix.minepainter.MinePainter;
import kentrix.minepainter.item.Manager;
import net.minecraft.world.World;

public class ManagerOperationHandler implements IMessageHandler<ManagerOperationMessage, IMessage>{
    @Override
    public IMessage onMessage(ManagerOperationMessage message, MessageContext ctx) {
        World world = ctx.getServerHandler().playerEntity.worldObj;
        MinePainter.log.info("Server received message " + this.getClass().getSimpleName());
        Manager.fillEntities(world, message.x, message.y, message.z, message.scale, message.url);
        return null;
    }
}
