package kentrix.minepainter.network;

import cpw.mods.fml.common.network.simpleimpl.IMessage;
import cpw.mods.fml.common.network.simpleimpl.IMessageHandler;
import cpw.mods.fml.common.network.simpleimpl.MessageContext;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

public class NBTUpdateHandler implements IMessageHandler<NBTUpdateMessage, IMessage>{

    private static MessageBuffer messageBuffer = null;


    @Override
    public IMessage onMessage(NBTUpdateMessage message, MessageContext ctx) {
        if(messageBuffer == null || !messageBuffer.getValidity()) {
            messageBuffer = new MessageBuffer(message.x, message.y, message.z, message.count,
                    message.of, message.bytes_data, ctx);
        }
        else {
            messageBuffer.concatMessage(message);
        }
        return null;
    }


}
