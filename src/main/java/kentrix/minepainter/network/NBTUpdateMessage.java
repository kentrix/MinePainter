package kentrix.minepainter.network;

import cpw.mods.fml.common.network.ByteBufUtils;
import cpw.mods.fml.common.network.simpleimpl.IMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import kentrix.minepainter.painting.PaintingEntity;
import net.minecraft.nbt.NBTTagCompound;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.ByteBuffer;
import java.util.*;

public class NBTUpdateMessage implements IMessage{

    int x;
    int y;
    int z;
    int count;
    int of;
    byte[] bytes_data;

    public static int BYTE_SPACE = 20000;

    public NBTUpdateMessage(){}

    public NBTUpdateMessage(int x, int y, int z, int count, int of, byte[] bytes){
        this.x = x;
        this.y = y;
        this.z = z;
        this.count = count;
        this.of = of;
        this.bytes_data = bytes;
    }

    public static List<NBTUpdateMessage> breakDownUpdateMessage(int x, int y, int z, BufferedImage bufferedImage) {
        List<NBTUpdateMessage> msgs = new ArrayList<NBTUpdateMessage>();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(bufferedImage, "png", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] bytes = baos.toByteArray();
        int i = 0;
        final int total = (int) Math.ceil((double)bytes.length / BYTE_SPACE) + (bytes.length == 0 ? 1 : 0);
        for(;;) {
            if(bytes.length < (i+1) * BYTE_SPACE) {
                // Last part
                msgs.add(new NBTUpdateMessage(x, y, z, i, total, Arrays.copyOfRange(bytes, i * BYTE_SPACE, bytes.length)));
                break;
            }
            else {
                msgs.add(new NBTUpdateMessage(x, y, z, i, total, Arrays.copyOfRange(bytes, i * BYTE_SPACE, (i+1) * BYTE_SPACE)));
            }
            i++;
        }
        return msgs;
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        this.x = buf.readInt();
        this.y = buf.readInt();
        this.z = buf.readInt();
        this.count = buf.readInt();
        this.of = buf.readInt();
        this.bytes_data = new byte[buf.readableBytes()];
        buf.readBytes(bytes_data);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(x);
        buf.writeInt(y);
        buf.writeInt(z);
        buf.writeInt(count);
        buf.writeInt(of);
        buf.writeBytes(bytes_data);
    }
}
